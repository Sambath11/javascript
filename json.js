var rectangle={
 color:'red',
 length:5.0,
 breadth:5.7,
 //property contains the function
 area:function(){
	 return this.length*this.breadth;
 }
};
function jsonEx(){
	console.log("The color of rectangle is "+rectangle.color);
	console.log("The length of rectangle is "+rectangle.length);
	console.log("The Breadth of rectangle is "+rectangle.breadth);
	console.log("The outline-color of rectangle is "+rectangle.outlineColor);
	console.log("The name of the rectangle is "+rectangle.name);
	//Adding the property for the object "Rectangle"
	rectangle["outlineColor"]="black";   //using syntax 1
	rectangle.name="Sample";   //using syntax 2
	//Trying to print the proprties again
	console.log("The outline-color of the rectangle is "+rectangle.outlineColor);
	console.log("The name of the rectangle is "+rectangle.name);
	//
	console.log("The area of the rectangle is "+rectangle.area());
	//
	console.log("The area of the rectangle is "+rectangle["area"]);
}
